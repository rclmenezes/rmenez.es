---
title: About
---

Hi,

I'm Rodrigo!

I'm the CTO and co-founder of SeedFi, a fintech startup aiming to make small dollar loans more impactful and ethical.

In this blog, I'll write about:

- Technologies I found useful
- Engineering management and how to scale a great technical organization
- The world of small dollar loans, and what we do at SeedFi

Before SeedFi, I was an early engineer at Moat, an adtech startup acquired by Oracle in 2017.

Any topic requests? Please include them in the comment section below!
