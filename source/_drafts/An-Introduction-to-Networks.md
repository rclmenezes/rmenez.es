---
title: A Brief Introduction to Networks
date: 2016/1/7
category:
- Tutorial
- Networks
---

In this post, we'll explore how computers talk to each other and how the internet actually works. It's worth noting that networks is a very large field, so we're not going to discuss anything at any great depth. Instead, we're going to cover this topic in broad strokes to gain a practical understanding for the average developer.

## The Physical Layer: how data gets transmitted

First, we should cover Network Interface Controllers (NIC), the hardware that computers use to communicate:

<span class="image-src">
[![A network interface card. Source: Creative Commons](/images/network_interface_card.jpg)](https://commons.wikimedia.org/wiki/File:Network_card.jpg)
</span>

Each NIC handles a specific "physical layer" of communication. For example, Ethernet network controllers have a physical plug where a cable is connected. They detect changes in voltage that in turn, represent bits. On the other hand, wireless NICs have antennas that create or detect changes in signal at a certain frequency.

These 1s and 0s that computers send to each other are called "packets": little digital envelopes that contain data. To sign their packets, NICs also provided hardcoded unique identifiers called _MAC Addresses_ (Media Access Control).

However, to make sense of these packets, the receiving computer needs to know a couple of things:
- Where in the packet is the sender's MAC Address? Which byte does it start/stop on?
- Who was the packet meant for? Is there a destination MAC address or is this meant for everyone?
- How long is this packet? A bunch of sequential zeros might be data, not the end of a message.
- Are we doing little endian or big endian decoding?
- etc.

Clearly, for this to work, we're going to need some common standards for machines to understand each other's packets.

## The Link Layer: Handling traffic within a network

Luckily for us, the IEEE (Institute of Electrical and Electronics Engineers) has been creating standards for the past few decades. For example, here's what an Ethernet II packet looks like:

- A preamble (7 bytes of alternating 10101010s used to synchronize receiver clocks)
- A start of frame delimiter (1 byte of 10101011 to break the pattern and signal the start of actual data)
- The destination MAC Address (6 bytes)
- The source MAC address (6 bytes)
- The length of the packet (2 bytes. More detail about this here)
- The actual payload of the message (46-1500 bytes)
- The frame check sequence (4 bytes. A checksum that allows the receiver to confirm that the message was sent properly)
- An interpacket sequence (12 bytes, a pause so the receiver can prepare for the next packet)

These protocols that describe how the physical layer should be used are called the "link layer".

Link Layer protocols also describe behavior. For example, in half-duplex Ethernet cables, computers can't receive and transmit on the same cable at the same time. This creates the potential for computers to transmit at the same, creating nonsense. Luckily, transmitters can detect when collisions happen so the Ethernet protocol also describes how much of a break each computer should take before trying again.

In case you're wondering about the 46 byte minimum for Ethernet payloads above, this is related to collision detection. Lets say it takes L time for a bit to go from one end of the network to the other. In a worst case scenario, where a bit is sent right before it is received, it could take 2L time for the transmitter to realize there's a collision. This is referred to as the "slot time" and 64 bytes (46 bytes + 18 header/frame check bytes) was calculated as a good minimum to make sure that a transmission takes longer than the slot time.

Link Layer protocols are used to transmit within a network. So what if we want to transmit to another network?

## The Internet Layer: IP and traffic between networks
Different networks can use link layer protocols. Thus, for messages to be sent across networks, we need a common protocol for everyone.

Enter IP: the Internet Protocol. The idea is, every device that sends/receives messages has a unique IP address to send/receive packets. The Internet Assigned Numbers Authority (IANA) allocates the address space between five regional Internet registries, who allocate between local Internet registries (ISPs). When your router connects to the Internet, it is assigned an IP Address.

Interestingly, when the Internet Protocol was made, no one expected the Internet to grow so quickly. So IPv4 addresses (which look like 127.0.0.1 - 32 bits) only have ~17 billion possibilities. When the Internet Engineering Task Force (IETF, which makes the IP Protocol standard) realized this, they started working on a new protocol called IPv6. IPv6 has 128 bits, which solves the address exhaustion problem and new routers suppport this format and have backwards compatibality for IPv4.

So how do routers get their IP address? And how do they know where their packets should be sent? The quick answer: voodoo magic. They have their own open protocols and you'll probably never have to worry about this. However, it's a fascinating subject and I may right about it later.

## The Transport Layer: TCP/UDP

Next, we have the transport layer! This is where stuff really gets good. Essentially, the transport layer's role is to:

_De-multiplex traffic_
If multiple processes are sending and receiving messages, then incoming messages need to indicate which process they were talking to. This is handled by "ports". When a message is sent out in TCP/UDP, it's always sent with a port number. The responding computer uses the same port number and it gets sent to the right process.

There's a limited port range (49152 to 65535 is what IANA recommends) and your operating system handles allocating all of that and error handling (for example, freeing the port if the other computer stops responding).

_Abstract away from the IP layer_
Let's say you're sending data from one computer to the other. You don't want to think about message payload limits and what not. The transport layer allow you to say "open a connection to ip_address:port and send them this". Easy!





To combat address exhaustion, most devices don't have an actual IP address. Instead, they have a "local address" and the router sends the message for your devices. When it receives the response, the router simply routes the message to the intended device. Pretty simple!

How does the data get copied over into main memory? Well, it depends on the NIC chip but most fire off an interrupt, telling the operating system "stop what you're doing I have new data for you to process."

WIFI DOS

The NIC handles all of this - decoding/encoding and even buffering. So how does data actually get into memory that's accessible by the CPU? Most NICs fire off operating system interrupts that tell the OS "hey I have a new packet ready for you".

https://commons.wikimedia.org/wiki/File:Internet-hourglass.svg

- What's handled by the driver?
- TCP vs UDP.
- How does DNS work? From Root name servers to normal name servers.
- How to use Route 53.
- SSL and certificates
socket and port
