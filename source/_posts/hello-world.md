---
title: Hello World
date: 2016/12/24
category: Random
---

I've started and abandoned many blogs in the past. Over time, most of them fizzeled out because I thought that I didn't have anything worth writing. If it wasn't original, I reasoned, I wouldn't be worth my time to write and worth the time of others to read.

Turns out, that's a crappy approach to writing because you never get better that way. Quantity generate quality in the long run.

So, I've created _this_ blog with a completely different goal in mind: write as often as possible, even if I'm writing about something pretty stupid. I think there'll be other benefits:

1. Hopefully, I'll learn to become more succinct in real life. I ramble a lot when speaking and it doesn't make me the best communicator.

2. In learning to write more clearly, maybe I'll learn to think more clearly. It's possible that good communication is just a symptom of a well-organized mind.

3. Having a written record of things my thoughts and opinions sound like it'll be nice in a few years [1].

4. Writing teaches empathy. The best writers make sure there is no ambiguity by placing themselves in the position of the reader. Understanding others is a skill that makes you more successful in, I believe, almost every field imaginable.

Let's see how far this goes!

[1] As a kid, I used to hate being photographed. It seemed forced to me. However, as I've grown older, I often wish I had more pictures.

_edit_. My plan to write frequently failed after 4 blog posts from 2016-2017. That's embarassing! I'm restarting this blog in 2020, so let's see if take two is more succesful.
