# TODO

## Chores

- Test that https://rmenez.es actually works?
- Review old posts and make sure they stand up

## Tutorial ideas

- Proper terraform management
- Chamber for secrets

## Engineering management

- Measurable approaches to interviews and reviews
- How to scale ownership in an engineering organization
- Defining "over-engineering"
- The 10x Engineer myth
- Why to hire local engineers
- When to hire consultants
- Pick boring technologies

##

- Make small pieces of code in C that send/receive from a port.
- Keep writing!
  - What are ports, anyway?
  - Public networking: Ethernet/Wi-Fi. Exponential back-off. WireShark, packet sniffing and more.
  - The IP bottleneck. A little bit about BGP and routing IP packets.
  - TCP vs UDP.
  - How does DNS work? From Root name servers to normal name servers.
  - How to use Route 53.
  - SSL and certificates

## Other potential topics

### Networks

- Ethernet/Wi-fi
- TCP/UDP
- IP/DNS

### Functional programming

- Lisps and fancy things like currying
- Influences on other languages

### Intro to ML

- Regressions and optimizations
- K-means clustering and unsupervised learning
- Expectation maximization and optimization
- Neural networks and normalization
- Multi-layer neural networks and RNNs

### Containers

- VMs vs Containers
- Intro to Docker and image registries
- Intro to Docker Compose and ECS Cli

### Low level shit

- Simple logic design
- Bootstrapper
- User vs. protected mode
- Virtual memory
- Organization of a program

### Bash tools!

- Stdin/out and pipes
- Grep
- Which
- Cut
- Sort

### Intro to SQL

- Basic SQL
- SQL in depth
- Indices
- Window functions, Mviews and other postgresql goodies!

### Elasticsearch

- ES under the hood
- Sharding
- Indexers and analyzers

### Other

- Random Python usefulness
- CDNs
