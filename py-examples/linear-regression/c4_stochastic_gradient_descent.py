# c4_stochastic_gradient_descent.py

import random
import sys

import matplotlib.pyplot as plt

from c2_plot_ny_wo_outliers import get_data_wo_outliers
from c3_batch_gradient_descent import estimate_price, plot_data_w_line


# Try playing with the learning rates!
THETA_0_LEARNING_RATE = 0.1
THETA_1_LEARNING_RATE = 0.1**10


def stochastic_gradient_descent(data, theta_0, theta_1):
    # End up with:
    # Iteration 8075, error: 1.01475127545e+13, thetas: 103991.20381 1141.75031008
    error = 0
    for x, y in data:
        hypothesis = estimate_price(theta_0, theta_1, x)
        loss = y - hypothesis

        error += 0.5 * (loss)**2
        theta_0 = theta_0 + THETA_0_LEARNING_RATE * (y - hypothesis)
        theta_1 = theta_1 + THETA_1_LEARNING_RATE * (y - hypothesis) * x
    return error, theta_0, theta_1


def linear_regression(data, gradient_descent_func):
    theta_0 = 0
    theta_1 = data[0][1] / data[0][0]
    last_error = sys.maxint
    error = 1
    i = 0
    while (abs(error - last_error) / error) > (0.1 ** 5):
        last_error = error
        random.shuffle(data)
        error, theta_0, theta_1 = gradient_descent_func(
            data, theta_0, theta_1
        )
        i += 1
        print 'Iteration {}, error: {}, thetas: {} {}'.format(i, error, theta_0, theta_1)

    return theta_0, theta_1


if __name__ == '__main__':
    data = get_data_wo_outliers()
    theta_0, theta_1 = linear_regression(data, stochastic_gradient_descent)
    plot_data_w_line(data, theta_0, theta_1)
