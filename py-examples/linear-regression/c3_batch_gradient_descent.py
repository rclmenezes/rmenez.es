# c3_batch_gradient_descent.py

import sys

import matplotlib.pyplot as plt

from c2_plot_ny_wo_outliers import get_data_wo_outliers


# Try playing with the learning rates!
THETA_0_LEARNING_RATE = 0.1
THETA_1_LEARNING_RATE = 0.1**4


def batch_gradient_descent(data, theta_0, theta_1):
    # End up with:
    # Iteration 316, error: 65662214541.7, thetas: 80241.5458922 1144.09527519
    N = len(data)
    error = 0
    theta_0_sum = 0
    theta_1_sum = 0
    for x, y in data:
        hypothesis = estimate_price(theta_0, theta_1, x)
        loss = y - hypothesis

        error += 0.5 * (loss) ** 2
        theta_0_sum += loss
        theta_1_sum += loss * x
    theta_0 += (THETA_0_LEARNING_RATE * theta_0_sum) / N
    theta_1 += (THETA_1_LEARNING_RATE * theta_1_sum) / N
    error /= N
    return error, theta_0, theta_1


def linear_regression(data, gradient_descent_func):
    theta_0 = 0
    theta_1 = data[0][1] / data[0][0]
    last_error = sys.maxint
    error = 1
    i = 0
    while (abs(error - last_error) / error) > (0.1 ** 7):
        last_error = error
        error, theta_0, theta_1 = gradient_descent_func(
            data, theta_0, theta_1
        )
        i += 1
        print 'Iteration {}, error: {}, thetas: {} {}'.format(i, error, theta_0, theta_1)

    return theta_0, theta_1


def estimate_price(theta_0, theta_1, x):
    return theta_0 + theta_1 * x


def plot_data_w_line(data, theta_0, theta_1):
    sqft, price = zip(*data)
    max_price, max_sqft = max(price), max(sqft)
    estimated_price = [estimate_price(theta_0, theta_1, x) for x in sqft]

    plt.plot(sqft, price, 'ro')
    plt.plot(sqft, estimated_price, 'b-')
    plt.axis([0, max_sqft, 0, max_price])
    plt.xlabel('sqft')
    plt.ylabel('price')
    plt.show()


if __name__ == '__main__':
    data = get_data_wo_outliers()
    theta_0, theta_1 = linear_regression(data, batch_gradient_descent)
    plot_data_w_line(data, theta_0, theta_1)
