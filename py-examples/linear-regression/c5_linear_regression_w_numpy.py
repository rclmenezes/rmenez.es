# c5_stochastic_gradient_descent.py

import random
import sys

import matplotlib.pyplot as plt
import numpy as np

from c2_plot_ny_wo_outliers import get_data_wo_outliers
from c3_batch_gradient_descent import estimate_price, plot_data_w_line


LEARNING_RATE = 0.1**6


def batch_gradient_descent(x, y, theta):
    # End up with:
    # Iteration 316, error: 65662214541.7, thetas: 80241.5458922 1144.09527519
    x_trans = x.transpose()
    M = len(x)
    hypothesis = np.sum(x * theta, axis=1)
    loss = hypothesis - y
    theta -= LEARNING_RATE * (np.sum(x_trans * loss, axis=1)) / M
    error = np.sum(0.5 * loss.transpose() * loss)
    return error, theta


def linear_regression(x, y, gradient_descent_func):
    theta = np.ones(np.shape(x[0]))
    last_error = sys.maxint
    error = 1
    i = 0
    while (abs(error - last_error) / error) > (0.1 ** 7):
        last_error = error
        error, theta = gradient_descent_func(
            x, y, theta
        )
        i += 1
        print 'Iteration {}, error: {}, theta: {}'.format(i, error, theta)

    return theta


def data_in_np(data):
    sqft, price = zip(*data)
    # Adding a 1 to replicate our \theta_0
    sqft = np.array([np.array([1, s]) for s in sqft])
    price = np.array(price)
    return sqft, price


if __name__ == '__main__':
    data = get_data_wo_outliers()
    sqft, price = data_in_np(data)
    theta = linear_regression(sqft, price, batch_gradient_descent)
    theta_0, theta_1 = theta
    plot_data_w_line(data, theta_0, theta_1)
