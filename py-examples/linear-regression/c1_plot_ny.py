# c1_plot_ny.py
import csv

import matplotlib.pyplot as plt


def get_data():
    with open('ny_sf_data.csv', 'rb') as csvfile:
        reader = csv.DictReader(csvfile)
        rows = [r for r in reader]

        ny_data = [
            (int(p['sqft']), int(p['price']))
            for p in rows
            if p['in_sf'] == '0'
        ]
        return ny_data


def plot_data(data):
    sqft, price = zip(*data)
    plt.plot(sqft, price, 'ro')
    plt.xlabel('sqft')
    plt.ylabel('price')
    plt.show()


if __name__ == '__main__':
    data = get_data()
    plot_data(data)
