# c2_plot_ny_wo_outliers.py

from c1_plot_ny import get_data, plot_data


def get_data_wo_outliers():
    ny_data = get_data()
    ny_data_wo_outliers = [
        (s, p) for s, p in ny_data
        if s < 2000 and p < 2000000
    ]
    return ny_data_wo_outliers


if __name__ == '__main__':
    data = get_data_wo_outliers()
    plot_data(data)
