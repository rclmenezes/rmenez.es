# c6_derivative_method.py

import random
import sys

import matplotlib.pyplot as plt
import numpy as np

from c2_plot_ny_wo_outliers import get_data_wo_outliers
from c3_batch_gradient_descent import estimate_price, plot_data_w_line


LEARNING_RATE = 0.1**6


def linear_regression(x, y):
    # End up with 82528.3036581 1141.67386409 :)
    x_trans = x.transpose()
    return np.dot(np.linalg.inv(np.dot(x_trans, x)), np.dot(x_trans, y))


def data_in_np(data):
    sqft, price = zip(*data)
    sqft = np.array([np.array([1, s]) for s in sqft])
    price = np.array(price)
    return sqft, price


if __name__ == '__main__':
    data = get_data_wo_outliers()
    sqft, price = data_in_np(data)
    theta = linear_regression(sqft, price)
    theta_0, theta_1 = theta
    print theta_0, theta_1
    plot_data_w_line(data, theta_0, theta_1)
